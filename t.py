from time import sleep
import sys
from mfrc522 import SimpleMFRC522
import OPi.GPIO as GPIO
import traceback

reader = SimpleMFRC522(board_type=GPIO.PCPCPLUS,pin_rst=29, pin_irq=31)

try:
    while True:
        print("Hold a tag near the reader")
        # wait for up to 5 seconds for a rising edge (timeout is in milliseconds)
        #channel = reader.Wait_for_event()
        #if channel is None:
        #  print('Timeout occurred on reader\n')
        id, text = reader.read()
        print("ID: %s\nText: %s" % (id,text))
        sleep(1)
        #else:
        #    print('Edge detected on channel\n', channel)
        #    id, text = reader.read()
        #    print("ID: %s\nText: %s" % (id,text))

except KeyboardInterrupt:
    print ("\nInterupted")

except ValueError:
   reader.Close_Reader()
   traceback.print_exc()
   print ("\nError.")

except TypeError:
   traceback.print_exc()
 
except NameError:
    traceback.print_exc()

finally:
   reader.Close_Reader()
   print ("\nBye.")
